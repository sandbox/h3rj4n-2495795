<?php

/**
 * @file
 * Contains the CFormsTestFormPage class.
 */

if (!class_exists('CFormsTestFormPage')) {
  require __DIR__ . '/CFormsTestFormPage.inc';
}

/**
 * Plugin class for the `testFormMenuMapping` test case.
 */
class CFormsTestFormArgumentsPage extends CFormsTestFormPage {

  /**
   * @var stdClass Form node.
   */
  private $node;

  /**
   * {@inheritdoc}
   */
  public function getForm(array $form, array &$form_state) {
    $form = parent::getForm($form, $form_state);

    // Change the default value with the node title.
    $form['test']['#default_value'] = $this->node->title;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormArguments(array $arguments = array()) {
    $this->node = $arguments[0];
  }
}
