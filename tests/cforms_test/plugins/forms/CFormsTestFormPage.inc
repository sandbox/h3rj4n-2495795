<?php

/**
 * @file
 * Contains the CFormsTestFormPage class.
 */

/**
 * Plugin class for the `testFormMenuMapping` test case.
 */
class CFormsTestFormPage extends CFormsBase {

  /**
   * Constructor.
   *
   * Disables exception handling because some actions
   * should fail, and shouldn't be handled as being
   * failed in simpletest.
   */
  public function __construct() {
    // Make sure that simple test doesn't collect this error.
    define('SIMPLETEST_COLLECT_ERRORS', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array $form, array &$form_state) {
    // A test field.
    $form['test'] = array(
      '#type' => 'textfield',
      '#title' => t('Test textfield'),
      '#default_value' => 'Found',
      '#name' => 'test-field',
    );

    $form['actions'] = array(
      '#type' => 'actions',
    );

    // A button without handler.
    $form['actions']['submit-no-handler'] = array(
      '#type' => 'submit',
      '#value' => t('No handler'),
      '#name' => 'no-submit-handler',
    );

    // A button that triggers a validation fail.
    $form['actions']['validation-fail'] = array(
      '#type' => 'submit',
      '#value' => t('Fail validation'),
      '#name' => 'validation-fail',
    );

    // A button that correctly submits the form.
    $form['actions']['submit-form'] = array(
      '#type' => 'submit',
      '#value' => t('Submit form'),
      '#name' => 'submit-form',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array $form, array &$form_state) {
    $return = TRUE;

    try {
      if (NULL !== ($callback_return = $this->dynamicCallback($form, $form_state, 'validateForm'))) {
        $return = $callback_return;
      }
    }
    catch (Exception $e) {
      $return = 'Exception';
      watchdog_exception('cforms_test', $e);
    }

    return $return;
  }

  /**
   * Triggers an validation fail.
   */
  protected function validateFormvalidation_fail(array $form, array &$form_state) {
    form_set_error('test', t('Validation failed'));
  }

  /**
   * Validation that can continue.
   */
  protected function validateFormsubmit_form(array $form, array &$form_state) {
    drupal_set_message(t('Validation success'));
    return TRUE;
  }

  /**
   * Submit handler.
   */
  protected function submitFormsubmit_form(array $form, array &$form_state) {
    drupal_set_message(t('Submit success'));
    return TRUE;
  }
}
