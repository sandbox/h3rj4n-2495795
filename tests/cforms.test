<?php

/**
 * @file
 * Tests for cforms.module.
 */

/**
 * Defines a base class for testing the CForms module.
 */
class CFormsWebTestCase extends DrupalWebTestCase {

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $modules = func_get_args();
    if (isset($modules[0]) && is_array($modules[0])) {
      $modules = $modules[0];
    }
    $modules[] = 'cforms';
    parent::setUp($modules);
  }

}

/**
 * Tests the mapping from a defined form into the plugin.
 */
class CFormsMappingTestCase extends CFormsWebTestCase {

  /**
   * Test case definition.
   */
  public static function getInfo() {
    return array(
      'name' => 'CForms mapping',
      'description' => 'Test the mapping from a form plugin.',
      'group' => 'CForms',
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp('cforms_test');

    $web_user = $this->drupalCreateUser();
    $this->drupalLogin($web_user);
  }

  /**
   * Run the mapping test.
   *
   * The test module defines a page defintion that can
   * be accessed to see whether the form was rendered
   * properly.
   */
  public function testFormMenuMapping() {
    // Go to the page.
    $this->drupalGet('cforms');
    $this->assertResponse(200);

    // The test field should be here.
    $this->assertFieldByName('test-field', 'Found');
  }
}

/**
 * Tests the mapping from a defined form into the plugin.
 *
 * Here aditional arguments are passed into the form.
 */
class CFormsMappingArgumentsTestCase extends CFormsWebTestCase {

  /**
   * Test case definition.
   */
  public static function getInfo() {
    return array(
      'name' => 'CForms argument mapping',
      'description' => 'Test the mapping from a form plugin with aditional arguments.',
      'group' => 'CForms',
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp('cforms_test');

    $web_user = $this->drupalCreateUser();
    $this->drupalLogin($web_user);
  }

  /**
   * Run the mapping test.
   *
   * The test module defines a page defintion that can
   * be accessed to see whether the form was rendered
   * properly.
   */
  public function testFormMenuMapping() {
    // Create a new node.
    $test_node = $this->drupalCreateNode();

    // Go to the page.
    $this->drupalGet("cforms/{$test_node->nid}");
    $this->assertResponse(200);

    // The test field should be here.
    $this->assertFieldByName('test-field', $test_node->title);
  }
}

/**
 * Tests the mappings into a plugin.
 */
class CFormsActionMappingFormTestCase extends CFormsWebTestCase {

  /**
   * Test case definition.
   */
  public static function getInfo() {
    return array(
      'name' => 'CForms form action mapping',
      'description' => 'Test the mapping of the form actions from a form plugin.',
      'group' => 'CForms',
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp(array(
      'dblog',
      'cforms_test',
    ));

    $web_user = $this->drupalCreateUser();
    $this->drupalLogin($web_user);
  }

  /**
   * Test hitting a button without handler.
   *
   * Buttons are proxied based upon the name of the button.
   * This test, will use a button without handler, this is
   * supposed to throw a run time execption.
   *
   * This exception is logged and the watchdog table is used
   * to verify that the correct error is thrown.
   */
  public function testNonExistentHandler() {
    // Go to the page.
    $this->drupalGet('cforms');
    $this->assertResponse(200);

    // No log yet.
    $wid = db_select('watchdog')
      ->fields(NULL, array('wid'))
      ->condition('type', 'cforms_test')
      ->condition('message', '%type: !message in %function (line %line of %file).')
      ->execute()
      ->fetchField();
    $this->assertFalse($wid);

    $this->drupalPost(NULL, array(), t('No handler'));

    $wid2 = db_select('watchdog')
      ->fields(NULL, array('wid'))
      ->condition('type', 'cforms_test')
      ->condition('message', '%type: !message in %function (line %line of %file).')
      ->execute()
      ->fetchField();
    $this->assertFalse(empty($wid2), t('Exception has been logged.'));
  }

  /**
   * Test form validation.
   */
  public function testValidationHandler() {
    // Go to the page.
    $this->drupalGet('cforms');
    $this->assertResponse(200);

    $this->drupalPost(NULL, array(), t('Fail validation'));

    // Should be in a message.
    $this->assertText(t('Validation failed'));

    // Clear the expected form error messages so they don't appear as exceptions.
    drupal_get_messages();
  }

  /**
   * Test form submission.
   */
  public function testSubmitHanlder() {
    // Go to the page.
    $this->drupalGet('cforms');
    $this->assertResponse(200);

    $this->drupalPost(NULL, array(), t('Submit form'));

    // Check the messages.
    $this->assertText(t('Validation success'));
    $this->assertText(t('Submit success'));
  }
}
