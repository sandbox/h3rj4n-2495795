<?php

/**
 * @file
 * Contains the CFormsBase class.
 */

/**
 * Class CFormsBase.
 *
 * The validation function is not required. This abstract class implements it so
 * that child classes inherit this.
 */
abstract class CFormsBase implements CFormsInterface {

  /**
   * @var array Adittional form arguments.
   */
  protected $form_args = array();

  /**
   * {@inheritdoc}
   */
  public function validateForm(array $form, array &$form_state) {
    $return = TRUE;

    // By default call a function based upon the clicked button.
    if (NULL !== ($callback_return = $this->dynamicCallback($form, $form_state, 'validateForm', TRUE))) {
      $return = $callback_return;
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   *
   * By default this function will call a submit callback based upon
   * the submit button that was clicked. When that callback doesn't
   * exist an exception will be thrown.
   *
   * Plugins can override this method when using a single submit handler.
   */
  public function submitForm(array $form, array &$form_state) {
    // By default call a function based upon the clicked button.
    $this->dynamicCallback($form, $form_state, 'submitForm');
  }


  /**
   * Get the ajax callback function.
   *
   * @return string
   */
  public function getAjaxCallback() {
    return 'cforms_plugin_ajax_callback';
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxCallback(array $form, array &$form_state) {
    $return = array();

    // By default call a function based upon the clicked button.
    if (NULL !== ($callback_return = $this->dynamicCallback($form, $form_state, 'ajaxCallback'))) {
      $return = $callback_return;
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormArguments(array $arguments = array()) {
    $this->form_args = $arguments;
  }

  /**
   * Creates the callback based upon a prefix.
   *
   * @param array $form
   * @param array $form_state
   * @param string $prefix
   * @param bool $allow_fail
   */
  protected function dynamicCallback(array $form, array &$form_state, $prefix, $allow_fail = FALSE) {
    $callback_return = NULL;

    if (!empty($form_state['clicked_button']['#name'])) {
      $clicked_button = $form_state['clicked_button']['#name'];
      $clicked_button_function = $prefix . preg_replace('~[^a-z]+~', '_', $clicked_button);

      $callback = array(
        $this,
        $clicked_button_function,
      );

      if (is_callable($callback)) {
        $params = array(
          $form,
          &$form_state,
        );

        $callback_return = call_user_func_array($callback, $params);
      }
      elseif (TRUE !== $allow_fail) {
        throw new RuntimeException(t('Tried to call a non-existing submit handler: @callback', array('@callback' => var_export($callback, TRUE))));
      }
    }

    return $callback_return;
  }

}
