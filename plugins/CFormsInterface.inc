<?php

/**
 * @file
 * Contains the CForm Interface.
 */

/**
 * Interface CFormInterface.
 */
interface CFormsInterface {

  /**
   * Get the renderable array of the form.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return array
   */
  public function getForm(array $form, array &$form_state);

  /**
   * Validate the form.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return bool
   */
  public function validateForm(array $form, array &$form_state);

  /**
   * Submit handler function for the form.
   *
   * @param array $form
   * @param array $form_state
   */
  public function submitForm(array $form, array &$form_state);

  /**
   * AJAX callback.
   *
   * When an AJAX callback should be performed from the sumit, then this
   * method is called. To use this, make sure that your AJAX callback is
   * defined as:
   *
   * @code
   * #ajax = array(
   *   'callback' => 'cforms_plugin_ajax_callback',
   * ),
   * @endcode
   *
   * @param array $form
   * @param array $form_state
   */
  public function ajaxCallback(array $form, array &$form_state);

  /**
   * Set arguments.
   *
   * A form can get additional passed in. This method sets these
   * additional parameters into the plugin object for later usage.
   *
   * @param array $arguments The additional form arguments.
   */
  public function setFormArguments(array $arguments = array());
}
